mod config;
mod error;
mod ui;
mod buffer;

fn main() {
    let config = match config::read_config() {
        Ok(cfg) => cfg,
        Err(err) => {
            eprintln!("An error occurred while reading the configuration file:");
            eprintln!("{}", err);
            std::process::exit(2);
        }
    };
    if let Err(err) = crate::ui::start(config) {
        eprintln!("An error occurred while running the editor:");
        eprintln!("{}", err);
        std::process::exit(1);
    }
}
