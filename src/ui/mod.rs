mod window;
mod screen;

use std::io;

use crate::config::Config;
use crate::error::*;
use self::screen::Screen;

pub fn start(cfg: Config) -> Result<()> {
    let writer = io::stdout();
    let writer = writer.lock();
    let (w, h) = termion::terminal_size()?;
    let size = (w as usize, h as usize);
    let mut screen = Screen::new(writer, size)?;

    Ok(())
}
