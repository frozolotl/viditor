use crate::buffer::Buffer;
use crate::config::Config;

pub struct Window<'a> {
    buffer: Box<dyn Buffer + 'a>,
    cfg: &'a Config,
}
