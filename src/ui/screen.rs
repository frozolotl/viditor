//! Renders the editor to the terminal.

use std::io::Write;
use termion::screen::AlternateScreen;
use termion::clear;

use crate::error::*;

const CH_EMPTY: char = ' ';

const LINE_V: char = '│';
const LINE_H: char = '─';
const LINE_D: char = '╷';
const LINE_U: char = '╵';
const LINE_R: char = '╶';
const LINE_L: char = '╴';

const LINE_LD: char = '┐';
const LINE_LU: char = '┘';
const LINE_RD: char = '┌';
const LINE_RU: char = '└';
const LINE_RV: char = '├';
const LINE_LV: char = '┤';
const LINE_HD: char = '┬';
const LINE_HU: char = '┴';
const LINE_HV: char = '┼';

pub struct Screen<W: Write> {
    /// Content is a vector with a length of `width * height`.
    /// A character is saved at `y * width + x`.
    content: Vec<char>,
    rerender: bool,
    buf: Vec<u8>,
    screen: AlternateScreen<W>,
    size: (usize, usize),
}

impl<W: Write> Screen<W> {
    pub fn new(writer: W, (w, h): (usize, usize)) -> Result<Screen<W>> {
        Ok(Screen {
            content: vec![CH_EMPTY; w * h],
            buf: Vec::new(),
            rerender: true,
            size: (w, h),
            screen: AlternateScreen::from(writer),
        })
    }

    /// Clears the screen.
    pub fn clear(&mut self) {
        for c in &mut self.content {
            *c = CH_EMPTY;
        }
        self.rerender = true;
    }

    /// Resizes and clears the screen.
    pub fn resize(&mut self, (w, h): (usize, usize)) {
        self.size = (w, h);
        self.clear();

        let wanted = w * h;
        let current = self.content.len();
        if wanted > current {
            let needed = wanted - current;
            self.content.reserve_exact(needed);
            self.content
                .extend(std::iter::repeat(CH_EMPTY).take(needed));
        } else {
            self.content.truncate(wanted);
        }

        self.rerender = true;
    }

    pub fn get(&self, (x, y): (usize, usize)) -> char {
        assert!(x < self.size.0 && y < self.size.1, "Out of screen bounds ({}, {}) >= ({}, {})", x, y, self.size.0, self.size.1);
        self.content[y * self.size.0 + x]
    }

    pub fn set(&mut self, (x, y): (usize, usize), ch: char) {
        assert!(x < self.size.0 && y < self.size.1, "Out of screen bounds ({}, {}) >= ({}, {})", x, y, self.size.0, self.size.1);
        self.content[y * self.size.0 + x] = ch;
        self.rerender = true;
    }

    /// Draws a line while handling intersections.
    /// The length is additive to x or y, depending on the orientation.
    pub fn draw_line(&mut self, pos: (usize, usize), length: usize, orientation: Orientation) {
        match orientation {
            Orientation::Vertical => self.draw_vline(pos, length),
            Orientation::Horizontal => self.draw_hline(pos, length),
        }
    }

    /// Draws a vertical line while handling intersections.
    /// The length is additive to y.
    pub fn draw_vline(&mut self, (x, y): (usize, usize), length: usize) {
        // the first part of the line should not have any parts which don't connect
        let first_pos = (x, y);
        let first = match self.get(first_pos) {
            self::LINE_LV | self::LINE_LU => LINE_LV,
            self::LINE_RV | self::LINE_RU => LINE_RV,
            self::LINE_HV | self::LINE_HU => LINE_HV,
            self::LINE_H | self::LINE_HD => LINE_HD,
            self::LINE_R | self::LINE_RD => LINE_RD,
            self::LINE_L | self::LINE_LD => LINE_LD,
            self::LINE_V | self::LINE_U => LINE_V,
            _ => LINE_D,
        };

        self.set(first_pos, first);
        for n in 1..length - 1 {
            let pos = (x, y + n);
            let current = self.get(pos);
            let next = match current {
                self::LINE_L | self::LINE_LV | self::LINE_LU | self::LINE_LD => LINE_LV,
                self::LINE_R | self::LINE_RV | self::LINE_RU | self::LINE_RD => LINE_RV,
                self::LINE_H | self::LINE_HU | self::LINE_HD | self::LINE_HV => LINE_HV,
                _ => LINE_V,
            };
            self.set(pos, next);
        }

        // the last part of the line should not have any parts which don't connect
        let last_pos = (x, y + length - 1);
        let last = match self.get(last_pos) {
            self::LINE_HV | self::LINE_HD => LINE_HV,
            self::LINE_LV | self::LINE_LD => LINE_LV,
            self::LINE_RV | self::LINE_RD => LINE_RV,
            self::LINE_H | self::LINE_HU => LINE_HU,
            self::LINE_R | self::LINE_RU => LINE_RU,
            self::LINE_L | self::LINE_LU => LINE_LU,
            self::LINE_V | self::LINE_D => LINE_V,
            _ => LINE_U,
        };
        self.set(last_pos, last);
    }

    /// Draws a horizontal line while handling intersections.
    /// The length is additive to x.
    pub fn draw_hline(&mut self, (x, y): (usize, usize), length: usize) {
        // the first part of the line should not have any parts which don't connect
        let first_pos = (x, y);
        let first = match self.get(first_pos) {
            self::LINE_HV | self::LINE_LV => LINE_HV,
            self::LINE_HU | self::LINE_LU => LINE_HU,
            self::LINE_HD | self::LINE_LD => LINE_HD,
            self::LINE_V | self::LINE_RV => LINE_RV,
            self::LINE_U | self::LINE_RU => LINE_RU,
            self::LINE_D | self::LINE_RD => LINE_RD,
            self::LINE_H | self::LINE_L => LINE_H,
            _ => LINE_R,
        };

        self.set(first_pos, first);
        for n in 1..length - 1 {
            let pos = (x + n, y);
            let current = self.get(pos);
            let next = match current {
                self::LINE_U | self::LINE_HU | self::LINE_LU | self::LINE_RU => self::LINE_HU,
                self::LINE_D | self::LINE_HD | self::LINE_LD | self::LINE_RD => self::LINE_HD,
                self::LINE_V | self::LINE_RV | self::LINE_LV | self::LINE_HV => self::LINE_HV,
                _ => LINE_H,
            };
            self.set(pos, next);
        }

        // the last part of the line should not have any parts which don't connect
        let last_pos = (x + length - 1, y);
        let last = match self.get(last_pos) {
            self::LINE_HV | self::LINE_RV => LINE_HV,
            self::LINE_HU | self::LINE_RU => LINE_HU,
            self::LINE_HD | self::LINE_RD => LINE_HD,
            self::LINE_V | self::LINE_LV => LINE_LV,
            self::LINE_U | self::LINE_LU => LINE_LU,
            self::LINE_D | self::LINE_LD => LINE_LD,
            self::LINE_H | self::LINE_R => LINE_H,
            _ => LINE_L,
        };
        self.set(last_pos, last);
    }

    /// Draws the current buffer to the screen.
    pub fn draw(&mut self) -> Result<()> {
        if self.rerender {
            self.buf.clear();
            for line in self.content.chunks(self.size.0) {
                for ch in line {
                    let mut buf = [0; 4];
                    let len = ch.encode_utf8(&mut buf).len();
                    self.buf.extend_from_slice(&buf[..len]);
                }
                self.buf.push(b'\n');
            }
            self.screen.write_all(&self.buf)?;
        }

        Ok(())
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Orientation {
    Vertical,
    Horizontal,
}
