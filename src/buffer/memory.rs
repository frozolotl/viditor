use super::{Buffer, Line};

/// The size of a page.
const PAGE_SIZE: usize = 1 << 10;

/// An estimate of the average line length to be used when allocating the buffer.
const AVG_LINE_LENGTH: usize = 40;

/// A buffer stored entirely in memory.
pub struct MemBuf<'a> {
    /// All the lines in this buffer.
    /// If a line is wrapped to multiple, the lines after that are None.
    /// This is used to have fast jumping to specific line numbers.
    lines: Vec<Option<Line<'a>>>,

    /// The position of the cursor in this buffer.
    position: (usize, usize),

    /// The width of the buffer
    width: usize,
}

impl<'a> MemBuf<'a> {
    /// Creates an empty buffer with a specified width.
    fn new(width: usize) -> MemBuf<'a> {
        MemBuf {
            lines: Vec::new(),
            position: (0, 0),
            width,
        }
    }

    /// Creates a buffer with predefined input and a specified width.
    fn from_string(input: &'a str, width: usize) -> MemBuf<'a> {
        let mut lines = Vec::with_capacity(input.len() / AVG_LINE_LENGTH);
        for s in input.lines() {
            let line = Line::new(s, width);
            let wraps = line.wraps().len();
            lines.push(Some(line));

            for _ in 0..wraps {
                lines.push(None);
            }
        }

        MemBuf {
            lines,
            position: (0, 0),
            width,
        }
    }
}

impl<'a> Buffer for MemBuf<'a> {
    fn position(&self) -> (usize, usize) {
        self.position
    }

    fn set_position(&mut self, pos: (usize, usize)) {
        self.position = pos;
    }

    fn lines<'b>(&'b self, start: usize) -> Box<dyn Iterator<Item = &'b Line<'b>> + 'b> {
        Box::new(Lines {
            buf: &self,
            index: start,
        })
    }
}

pub struct Lines<'a> {
    buf: &'a MemBuf<'a>,
    index: usize,
}

impl<'a> Iterator for Lines<'a> {
    type Item = &'a Line<'a>;

    fn next(&mut self) -> Option<&'a Line<'a>> {
        match self.buf.lines.get(self.index) {
            Some(Some(line)) => {
                self.index += self
                    .buf
                    .lines
                    .iter()
                    .skip(self.index)
                    .take_while(|line| line.is_none())
                    .count();
                Some(line)
            }
            _ => None,
        }
    }
}
