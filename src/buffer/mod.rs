use std::borrow::Cow;
use unicode_width::UnicodeWidthChar;

mod memory;

pub trait Buffer {
    /// The position of the cursor in this buffer.
    fn position(&self) -> (usize, usize);

    /// Set the position of the cursor in this buffer.
    fn set_position(&mut self, pos: (usize, usize));

    /// Iterates over the lines, starting at a specific location.
    fn lines<'a>(&'a self, start: usize) -> Box<dyn Iterator<Item = &'a Line<'a>> + 'a>;
}

pub struct Line<'a> {
    buf: Cow<'a, str>,

    /// The byte positions of line wraps.
    wraps: Vec<usize>,
}

impl<'a> Line<'a> {
    /// Creates a new line with specified buffer width.
    /// Wraps are applied.
    pub fn new(s: &'a str, width: usize) -> Line<'a> {
        let mut line = Line {
            buf: s.into(),
            wraps: Vec::new()
        };
        line.rewrap(width);

        line
    }

    pub fn rewrap(&mut self, max_width: usize) {
        self.wraps.clear();

        let mut render_width = 0;
        for (i, c) in self.buf.char_indices() {
            render_width += c.width().unwrap_or(0);
            if render_width > max_width {
                self.wraps.push(i);
                render_width = 0;
            }
        }
    }

    pub fn wraps(&self) -> &[usize] {
        &self.wraps
    }
}
