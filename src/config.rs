use crate::error::*;
use serde::{Deserialize, Serialize};
use std::{fs::{create_dir_all, File}, io};

use directories::ProjectDirs;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Config;

/// Reads the configuration file or creates one if none was found.
/// Exits with status code 1 if the home directory cannot be found.
pub fn read_config() -> Result<Config> {
    let project_dirs = project_dirs();
    let dir = project_dirs.config_dir();
    let path = dir.to_path_buf()
        .join(env!("CARGO_PKG_NAME"))
        .with_extension("yml");

    match File::open(&path) {
        Ok(file) => Ok(serde_yaml::from_reader(file)?),
        Err(ref err) if err.kind() == io::ErrorKind::NotFound => {
            create_dir_all(dir)?;
            let file = File::create(path)?;
            let cfg = Config::default();
            serde_yaml::to_writer(file, &cfg)?;
            Ok(cfg)
        }
        Err(err) => Err(err.into()),
    }
}

/// Returns the project directories for this crate.
/// Exits with status code 1 if the home directory cannot be found.
pub fn project_dirs() -> ProjectDirs {
    match ProjectDirs::from("", "", env!("CARGO_PKG_NAME")) {
        Some(v) => v,
        None => {
            eprintln!("Could not find home directory.");
            std::process::exit(1);
        }
    }
}
